from choices import Choices
from typing import List
from translation import Translation
import PySimpleGUI as sg


# The setup
TR: Translation = Translation('EN')
CH: Choices = Choices(True, translation=TR)


# Functions

def text(kw: str) -> str:
    res = TR.text(kw)
    return res


def message(title: str, texts: List):
    sg.popup(*texts, title=title)


def go(val):
    """Treatment when the "..." button is released
    """
    source = val['-SOURCE-']

    error, res, current = CH.result(source).detail_with_desc()
    if error:
        message(title=text('INP_KO'), texts=[error])
        return

    window['-M_RES-'].update(res)
    if current:
        window['-T_RES-'].update(text('RES').format(current))
    else:
        window['-T_RES-'].update('')


def move(step):
    """Treatment when the "Prev" or "Next" button is released : move backward (-1) or forward (1)
    :param step : -1 (Prev) or 1 (Next)
    """
    error, res, new = CH.move(step).detail_with_desc()
    if error:
        message(title=text('ERR'), texts=[error])
        return

    if new:
        window['-SOURCE-'].update('')
        window['-T_RES-'].update(text('RES').format(new))
        window['-M_RES-'].update(res)


# The main window
sg.theme('BlueMono')
layout = [[sg.T(text('EKW')), sg.In(k='-SOURCE-')],
          [sg.B('...', k='-...-'), sg.B(text('PRV'), k='-PRV-'), sg.B(text('NXT'), k='-NXT-'),
           sg.B(text('HLP'), k='-HLP-')],
          [sg.T('', size=(60, 1), k='-T_RES-')],
          [sg.Multiline('', disabled=True, size=(60, 15), k='-M_RES-')],
          [sg.B(text('EXI'), k='-EXI-')]]
window = sg.Window(text('CHO'), layout)


# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == '-EXI-':  # if user closes window or clicks "Exit"
        break

    # print('event : ', event)
    # print('values : ', values)

    if event == '-...-':
        # Show the result for the keyword
        go(values)
    elif event == '-PRV-':
        move(-1)
    elif event == '-NXT-':
        move(1)
    elif event == '-HLP-':
        message(text('HLP'), [text('HLP_M')])


window.close()
