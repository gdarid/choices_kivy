from typing import Dict
import json


class Translation:
    """Translations : texts in different languages (see text files in data/config/...)
    """

    def __init__(self, language: str = 'EN'):
        self.language: str = language or 'EN'
        self.dic: Dict[str, str] = {}
        self.error = ''
        try:
            with open(f"data/config/lang_{language}.json", mode='r', encoding='utf_8') as json_data:
                self.dic = json.load(json_data)
        except IOError:
            self.error = 'IO'
        except json.decoder.JSONDecodeError:
            self.error = 'JSON'
        except Exception as exc:
            self.error = f'{type(exc).__name__}'

    def text(self, kw: str, *args) -> str:
        if self.error != '':
            return self.error + '!'
        res = self.dic.get(kw, f'Translation not defined {kw}')
        if args:
            if kw in self.dic.keys():
                res = res.format(*args)

        return res
