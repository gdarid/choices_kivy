# Choices Kivy

This simple application is used to generate randomly sequences and dice results.

## User interfaces

The main user interface is Kivy and the corresponding main file is main.py

An other user interface (with same features) using PySimpleGUI can be seen with the file main_ch_psg.py


## Use

For a dice with 6 faces:
6

For a dice with 20 faces:
20

For a sequence of 10 items (from 1 to 10) with id 1:
10s1

To get the result after your input, push the ... button"


## Build

An APK (Android) can be built using buildozer
