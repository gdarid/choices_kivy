# main.py
from choices import Choices
from translation import Translation

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
from kivy.uix.label import Label
import json


class FirstWindow(Screen):
    inputcmd = ObjectProperty(None)
    res1 = ObjectProperty(None)
    res_id = ObjectProperty(None)

    def __init__(self, **kwargs):
        # New properties can be added here
        super(FirstWindow, self).__init__(**kwargs)

    @staticmethod
    def message(title, text):
        pop = Popup(title=title,
                    content=Label(text=text),
                    size_hint=(0.8, 0.8))
        pop.open()

    @staticmethod
    def text(kw: str) -> str:
        res = TR.text(kw)
        return res

    def goBtn(self):
        """Treatment when the "..." button is released
        """
        source = self.inputcmd.text

        error, res, current = CH.result(source).detail_with_desc()
        if error:
            self.message(title=TR.text('INP_KO'), text=error)
            return

        self.res1.text = res
        if current:
            self.res_id.text = TR.text('RES', current)
        else:
            self.res_id.text = ''

    def moveChoice(self, step):
        """Treatment when the "Prev" or "Next" button is released : move backward (-1) or forward (1)
        :param step : -1 (Prev) or 1 (Next)
        """
        error, res, new = CH.move(step).detail_with_desc()
        if error:
            self.message(title=TR.text('ERR'), text=error)
            return

        if new:
            self.inputcmd.text = ''
            self.res1.text = res
            self.res_id.text = TR.text('RES', new)

    def help(self):
        """Treatment when the "Help" button is released
        """
        self.message(title=TR.text('HLP'), text=TR.text('HLP_M'))


class ErrorWindow(Screen):
    msg_error = ObjectProperty(None)

    def __init__(self, msg, **kwargs):
        # New properties can be added here
        super(ErrorWindow, self).__init__(**kwargs)
        self.msg_error.text = msg  # 'Msg error'

    @staticmethod
    def text(kw: str) -> str:
        res = TR.text(kw)
        return res


class WindowManager(ScreenManager):
    pass


class MyMainApp(App):
    def build(self):
        return SM


if __name__ == "__main__":
    lang: str = ''
    msg_error: str = ''
    try:
        with open(f"data/config/config.json", mode='r', encoding='utf_8') as json_data:
            dic = json.load(json_data)
            lang = dic.get('LANG', 'EN')
    except IOError:
        msg_error = 'Config error : IO'
    except json.decoder.JSONDecodeError:
        msg_error = 'Config error : JSON'
    except Exception as exc:
        msg_error = f'Config error : {type(exc).__name__}'

    KV = Builder.load_file("main.kv")
    SM = WindowManager()
    TR: Translation = Translation(lang)
    CH: Choices = Choices(large_gui=False, translation=TR)

    if msg_error != '':
        screens = [ErrorWindow(msg_error, name="error")]
        for screen in screens:
            SM.add_widget(screen)
        SM.current = "error"
    else:
        screens = [FirstWindow(name="first"), ErrorWindow('', name="error")]
        for screen in screens:
            SM.add_widget(screen)
        SM.current = "first"

    MyMainApp().run()
