import unittest
from choices import Choices


class TestChoices(unittest.TestCase):
    def setUp(self):
        """Before each test (test_...)"""
        # print('setUp')
        self.info = ''

    def tearDown(self):
        """After each test (test_...)"""
        # print('tearDown')
        # Print specific test information (this printing could be mixed with the automatic printing of unitest)
        if self.info:
            print(f'Info :\n{self.info}\n')

    def test_result_progress(self):
        """Test the result function on a simple progress """
        ch = Choices()
        last, res = '', ''
        for li in range(15):
            error, res, current = ch.result('g0').detail()
            self.assertEqual(current, 'g0')

            if li <= 9:
                # Ten first results should be not empty and different
                self.assertNotEqual(res, '')
                self.assertNotEqual(res, last)
            else:
                # Other results ( >9 ) should be the same as the last returned "res" value
                self.assertEqual(res, last)

            self.assertEqual(error, '')
            last = res

        # self.info = res

    def test_result_no_progress(self):
        """Test the result function on a no progress test case"""
        ch = Choices()
        error, res, current = ch.result('!g0').detail()
        self.assertEqual(error, '')
        self.assertEqual(res, '')
        self.assertEqual(current, 'g0')

    def test_move(self):
        """Test the move function on a simple navigation """

        # Values for 0, 10, 5
        ch = Choices()
        error, res0, current = ch.result('g0').detail()
        error, res10, current = ch.result('g10').detail()
        error, res5, current = ch.result('g5').detail()

        # go "prev" to 0
        error, resmove, current = ch.move(-1).detail()
        self.assertEqual(error, '')
        self.assertEqual(resmove, res0)
        self.assertEqual(current, 'g0')

        # go "prev" again : nothing new
        error, resmove, current = ch.move(-1).detail()
        self.assertEqual(error, '')
        self.assertEqual(resmove, '')
        self.assertEqual(current, '')

        # go "next" (normally to 5)
        error, resmove, current = ch.move(1).detail()
        self.assertEqual(error, '')
        self.assertEqual(resmove, res5)
        self.assertEqual(current, 'g5')

        # go "next" (normally to 10)
        error, resmove, current = ch.move(1).detail()
        self.assertEqual(error, '')
        self.assertEqual(resmove, res10)
        self.assertEqual(current, 'g10')

        # go "next" again : nothing new
        error, resmove, current = ch.move(1).detail()
        self.assertEqual(error, '')
        self.assertEqual(resmove, '')
        self.assertEqual(current, '')


if __name__ == '__main__':
    unittest.main()
