"""This module gives the possibility to generate random results on chosen "choices"
"""
from typing import Tuple, List, Dict
from dataclasses import dataclass
from translation import Translation

import json
import random as rnd
import re


@dataclass
class ChoiceResult:
    """
    The result of a "choice"
    """
    error: str
    display: str
    chtid: Tuple[str, int]
    tr: Translation

    def detail(self) -> Tuple[str, str, str]:
        """Detail (for unpacking) with no description

        :return: (error, display, current)
            error : possible error message
            display : string to display
            current : current "choice" (as a str) if it is defined
        """
        if self.chtid[0]:
            current = self.chtid[0] + str(self.chtid[1])
        else:
            current = ''
        return self.error, self.display, current

    def detail_with_desc(self) -> Tuple[str, str, str]:
        """Detail (for unpacking) with a description of the current choice
        
        :param
        :return: (error, display, current)
            error : possible error message
            display : string to display
            current : current "choice" with a description if it is defined
        """
        if self.chtid[0].startswith('*'):
            # Dice with {} faces
            current = self.tr.text('REG_D', self.chtid[0][1:])
        elif self.chtid[0] == 'g':
            # Giant
            current = self.tr.text('REG_G', str(self.chtid[1]))
        elif self.chtid[0].startswith('s'):
            # Sequence
            current = self.tr.text('REG_S', str(self.chtid[1]), self.chtid[0][1:])
        else:
            current = ''
        return self.error, self.display, current


class Choices:
    """Keep and manage the different results
    """
    cstore: Dict[str, List]
    gchoices: List[str]
    currentpos: str
    large_gui: bool

    def __init__(self, large_gui: bool = False, translation: Translation = None):
        # Each value in cstore is a list of 3 items ( 0 : "done choice" - 1 : "cptr" - 2 : "current display" )
        self.cstore = {}
        self.currentpos = ''
        self.large_gui = large_gui
        if translation is None:
            translation = Translation('EN')
        self.tr = translation

        # Get the g choices
        try:
            with open(r"data/config/choices_g.json", mode='r', encoding='utf_8') as json_data:
                self.gchoices = json.load(json_data)
        except IOError:
            self.gchoices = [self.text('CONF_ND')]  # 'Config not defined'
        except json.decoder.JSONDecodeError:
            self.gchoices = [self.text('CONF_NDJ')]  # 'JSON config not correctly defined'
        except Exception as exc:
            self.gchoices = [type(exc).__name__]

    def text(self, kw: str, *args) -> str:
        """Get the text based on a given keyword
        :param kw: keyword
        :return: full text
        """
        return self.tr.text(kw, *args)

    def set_first_sample(self, chtype: str, intsource: int):
        """Set the first sample of a "chtype", only for 'g' and 's'

        :param chtype: the type of choice
        :param intsource : the number of the choice
        :return:
        """
        if chtype == 'g':
            dd = rnd.sample(self.gchoices, len(self.gchoices))
        else:
            # Sequence
            nbfaces = int(chtype[1:])
            dd = rnd.sample(range(1, nbfaces + 1), nbfaces)
            dd = [str(elem) for elem in dd]

        kst = self.keystore(chtype, intsource)
        self.cstore[kst] = [dd, -1, []]

    @staticmethod
    def keystore(chtype: str, intsource: int) -> str:
        """Returns the key for cstore as a combination of the parameters

        :param chtype: the type of choice
        :param intsource : the number of the choice
        :return: the key
        """
        return chtype + '_' + ('00000' + str(intsource))[-5:]

    @staticmethod
    def join_lf(sep: str, source, nb: int) -> str:
        """Join items with a possible line feed every nb items
        :param sep: separator
        :param source: source to join (list, ...)
        :param nb: maximum number of items in each line (0 for no limit) - it must be a positive integer
        :return: concatenation of the strings in the iterable source ...
        """
        if nb == 0:
            return sep.join(source)

        if nb > 0:
            res = ''
            for pos, item in enumerate(source):
                if pos == 0:
                    res = str(item)
                else:
                    res = res + sep
                    if pos % nb == 0:
                        res = res + '\n'
                    res = res + str(item)

            return res

        raise NotImplementedError()

    def error_result(self, error: str) -> ChoiceResult:
        """Simple result with error message
        :param error: error message
        :return: ChoiceResult(error, display, chtid)
            error : error message
            display : string to display
            chtid : (chtype,num)
        """
        return ChoiceResult(error, '', ('', -1), self.tr)

    def choice_result(self, error: str, display: str, chtid: Tuple[str, int]) -> ChoiceResult:
        """Choice result
        :param error: possible error message
        :param display: text to display
        :param chtid : Tuple[str, int]
        :return: ChoiceResult(error, display, chtid)
        """
        return ChoiceResult(error, display, chtid, self.tr)

    def result(self, source: str) -> ChoiceResult:
        """A source ("a choice") is asked for a result
        :param source: choosen source
            Modifiers are possibly at the beginning and/or end
            Optional modifiers at the beginning
                ! : no "progress", just give back the current display for "source"
                [number of faces] (mandatory for a dice and optional for a sequence)
            Core (the choice type (chtype)) :
                g : "a giant"
                s : a sequence
                for the dice, nothing here
            Modifiers at the end, for g and s only
                [num] : an integer to identify the "choice"

            examples :
                '10s1', 's2' , '20', 'g1'
                '!10s1', '!s2' , '!g1'  (! at the beginning to have only current value)

        :return: ChoiceResult(error, display, chtid)
            error : possible error message
            display : string to display
            chtid : (chtype,num)
        """
        progress: bool = True

        if not source:
            return self.error_result('')

        # Parse source
        regex: str = r"^(!?)(\d*)([gs]*)(\d*)$"
        match = re.search(regex, source)
        if not match:
            # 'Incorrect input'
            return self.error_result(self.text('INP_KO'))

        m_prog, m_faces, m_chtype, m_num = match.group(1), match.group(2), match.group(3), match.group(4)

        if m_faces and m_chtype == 'g':
            # g with a number before is not possible
            return self.error_result(self.text('INP_G'))

        if not m_num and m_chtype == 's':
            # "X" must be followed by an identification number
            return self.error_result(self.text('INP_X_AFT', 's'))

        if not m_num and m_chtype == 'g':
            # "X" must be followed by an identification number
            return self.error_result(self.text('INP_X_AFT', 'g'))

        if m_faces:
            nb = int(m_faces)
            if nb == 0 or nb > 1000:
                # The first number must be between 1 and 1000
                return self.error_result(self.text('INP_NB1'))

        chtype: str
        if m_chtype == 'g':
            chtype = m_chtype
        elif m_chtype == 's':
            chtype = 's' + (m_faces if m_faces else '20')
        else:
            chtype = '*' + m_faces

        if m_prog:
            progress = False

        if m_num:
            intsource = int(m_num)
            if intsource > 1000:
                # The identification number must be between 0 and 1000
                return self.error_result(self.text('INP_NB2'))
        else:
            intsource = -1000

        kst: str = self.keystore(chtype, intsource)  # kst : simple key for the choice (to be used in the cstore)
        self.currentpos = kst
        chtid = (chtype, intsource)  # chtid : combined Id for the choice (used by choice_result)
        if chtype == 'g':
            sep = '\n'
            line_nb = 0
        else:
            sep = ','
            line_nb = 0 if self.large_gui else 5

        # No progress
        if not progress:
            if chtype.startswith('*'):
                # Nothing to return, in this case, for a simple dice
                return self.choice_result('', '', chtid)

            # Else just give back the current display for chtid
            if kst not in self.cstore.keys():
                # A new sample in cstore and return nothing
                self.set_first_sample(chtype, intsource)
                return self.choice_result('', '', chtid)

            dispjoin = self.join_lf(sep, self.cstore[kst][2], line_nb)
            return self.choice_result('', dispjoin, chtid)

        # With progress
        if chtype.startswith('*'):
            # Simple dice : simple storage
            nbfaces: int = int(chtype[1:])
            res: str = str(rnd.randint(1, nbfaces))
            self.cstore[kst] = [[res], 0, [res]]
            return self.choice_result('', res, chtid)

        # Not a simple dice
        if kst not in self.cstore.keys():
            # A new sample in cstore
            self.set_first_sample(chtype, intsource)

        cstval = self.cstore[kst]  # cstval : the "V" (value) in the cstore dict
        currptr = cstval[1] + 1

        if currptr < len(cstval[0]):
            # the new value is added
            cstval[1] = currptr
            display = cstval[0][currptr]
            cstval[2].append(display)

        dispjoin = self.join_lf(sep, cstval[2], line_nb)

        return self.choice_result('', dispjoin, chtid)

    def move(self, step) -> ChoiceResult:
        """
        :param step : -1 (Prev) or 1 (Next)
        :return: ChoiceResult(error, display, chtid)
            error : possible error message
            display : string to display
            chtid : (chtype, num) of the "new" item only if the item changed
        """
        if self.currentpos == '':
            return self.error_result('')

        lkeys = list(self.cstore.keys())
        lkeys.sort()

        if self.currentpos not in lkeys:
            # Navigation error
            return self.error_result(self.text('ERR_NAV'))

        idx = lkeys.index(self.currentpos)
        if idx + step < 0 or idx + step >= len(lkeys):
            # Nothing in the chosen direction
            return self.error_result('')

        chtype = lkeys[idx+step][0]
        if chtype == 'g':
            sep = '\n'
            line_nb = 0
        else:
            sep = ','
            line_nb = 0 if self.large_gui else 5

        dispjoin = self.join_lf(sep, self.cstore[lkeys[idx+step]][2], line_nb)
        self.currentpos = lkeys[idx+step]

        # Parse currentpos for (chtype, intsource)
        try:
            chtype, intstr = self.currentpos.split('_')  # See keystore for key composition
            intsource = int(intstr)
        except ValueError:
            return self.error_result(self.text('ERR_NAV'))

        return self.choice_result('', dispjoin, (chtype, intsource))


if __name__ == "__main__":
    CH: Choices = Choices()
